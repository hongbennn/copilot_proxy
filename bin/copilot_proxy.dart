import 'package:copilot_proxy/copilot_proxy.dart' as copilot_proxy;

Future<void> main(List<String> args) => copilot_proxy.main(args);