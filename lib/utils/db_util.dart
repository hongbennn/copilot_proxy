import 'package:copilot_proxy/check_auth/access_token_check_auth.dart';
import 'package:copilot_proxy/common.dart';
import 'package:copilot_proxy/utils/json_util.dart';
import 'package:path/path.dart' as path;
import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';

const _databaseVersion = 1;
final _parentDirPath = path.dirname(config.configFilePath!);
late Database _db;
late final StoreRef<String, JsonMap> users;

Future<void> loadDB() async {
  final dbPath = path.join(_parentDirPath, 'data.db');
  users = stringMapStoreFactory.store('users');
  _db = await databaseFactoryIo.openDatabase(
    dbPath,
    version: _databaseVersion,
    onVersionChanged: (db, oldVersion, newVersion) async {
      if (oldVersion == 0) {
        _db = db;
        await initDB(db);
      }
    },
  );
}

Future<void> initDB(Database db) async {
  JsonMap? upJsonMap = await loadJson('username_password.json');
  JsonMap oldUserJsonMap = await loadJson('user.json');
  //init admin user
  final adminUsername = 'admin';
  final adminRecord = users.record(adminUsername);
  if (!await adminRecord.exists(db)) {
    await addUser(adminUsername, config.adminPassword);
  }
  //merge old user data
  for (final JsonMap userJson in oldUserJsonMap.values) {
    final String username = userJson['login'];
    final String? password = upJsonMap?.remove(username);
    if (password != null) userJson['password'] = password;
    await users.record(username).add(db, userJson);
  }
  //new user data
  for (final e in (upJsonMap?.entries ?? <MapEntry<String, String>>[])) {
    await addUser(e.key, e.value);
  }
  await deleteJson('user.json');
  await deleteJson('username_password.json');
  await deleteJson('access_token.json');
  await deleteJson('client_ip.json');
}

Future<void> saveDB() => _db.close();

Future<JsonMap?> addUser(
  String username,
  String password,
) async {
  final userJson = generateFakeUser(username: username, password: password);
  await users.record(username).add(_db, userJson);
  return userJson;
}

Future<void> saveUser(String username, JsonMap update) => users.record(username).update(_db, update);

Future<JsonMap?> getUser(String? username) async {
  if (username == null) return null;
  return await users.record(username).get(_db);
}

Future<JsonMap?> getUserByAccessToken(String? accessToken) async {
  if (accessToken == null) return null;
  final finder = Finder(filter: Filter.equals('accessToken', accessToken));
  final userRecord = await users.findFirst(_db, finder: finder);
  return userRecord?.value;
}
